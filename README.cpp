   1   while (ok()) {
   2   std::cout << "=============================before read odometry=====================" << std::endl;
   3     //read left&right encoder
   4 
   5     int left_enc = get_axis_position(robot_name, 7);
   6     int right_enc = get_axis_position(robot_name, 8);
   7     //std::cout << "########## " << left_enc << ' ' << right_enc << std::endl;
   8     //calculate odometry
   9     diff_drive_->Update(get_time(), left_enc, right_enc);
  10 
  11     //publish odom
  12     nav_msgs::msg::Odometry odom;
  13     odom.pose.pose.position.x = diff_drive_->GetPose(0);
  14     odom.pose.pose.position.y = diff_drive_->GetPose(1);
  15     auto q = diff_drive_->GetQuat();
  16     odom.pose.pose.orientation.x = std::get<0>(q);
  17     odom.pose.pose.orientation.y = std::get<1>(q);
  18     odom.pose.pose.orientation.z = std::get<2>(q);
  19     odom.pose.pose.orientation.w = std::get<3>(q);
  20     odom.twist.twist.linear.x = diff_drive_->GetTwist(0);
  21     odom.twist.twist.angular.z = diff_drive_->GetTwist(1);
  22 
  23   std::cout << "=============================before pub odom=====================" << std::endl;
  24     io_->Publish<nav_msgs::msg::Odometry>("odom", odom);
  25   std::cout << "=============================after pub odom=====================" << std::endl;
  26     std::cout << "odom.x:" << odom.pose.pose.position.x << std::endl;
  27 
  28     rt.Spin();
  29   }
  30 
  31   return 0;
  32 }



以下为启动日志和异常
=============================1=====================
start testing IO board
debug 1
debug 2
i_CPos=-4.654588,-252.578554,l
dof=8
enter interp() 
power is finish!
=============================2=====================
=============================3=====================
=============================4=====================
=============================before read odometry=====================
=============================before pub odom=====================
=============================after pub odom=====================
odom.x:0
=============================before read odometry=====================
=============================before pub odom=====================
=============================after pub odom=====================
odom.x:0
#########robot_name ec_device1/robot0 
operation mode: 0 
Status Word: 1237 
init_pos1:-146191,int_pos2-52183,int_pos3-162649,int_pos4-406467,int_pos5-438405pos6-259476
*** Aborted at 1640850370 (unix time) try "date -d @1640850370" if you are using GNU date ***
=============================before read odometry=====================
=============================before pub odom=====================
=============================after pub odom=====================
odom.x:-1.87254e-08
PC: @                0x0 (unknown)
*** SIGSEGV (@0x0) received by PID 13399 (TID 0x7f719e7fc700) from PID 0; stack trace: ***
    @     0x7f71ddd2f2c6 google::(anonymous namespace)::FailureSignalHandler()
    @     0x7f71dddc13c0 (unknown)
    @     0x7f71ddaf6d27 (unknown)
    @     0x7f71ddaf7615 _Unwind_Resume
    @     0x7f71cc1f2bac _ZNSt17_Function_handlerIFvSt10shared_ptrIKN13geometry_msgs3msg6Twist_ISaIvEEEEEZN10AmrControl10InitializeERKN3amr11amr_maestro12ModuleConfigES0_INSA_2io9IOAdaptorEEEUlT_E_E9_M_invokeERKSt9_Any_dataOS7_.cold
    @                0x0 (unknown)
I1230 15:46:10.699615 13398 slam_maestro.cpp:275] From container thread: process 13399 exited with ec: 139
I1230 15:46:10.699633 13398 slam_maestro.cpp:283] Process 13399 is being erased from managed process list.

